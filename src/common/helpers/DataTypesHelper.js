import fn from 'helpers/dataTypesHelpers/FunctionHelper';
import array from 'helpers/dataTypesHelpers/ArrayHelper';
import obj from 'helpers/dataTypesHelpers/ObjectHelper';
import str from 'helpers/dataTypesHelpers/StringHelper';

const dataTypes = {
	fn,
	array,
	obj,
	str
};

export {
	fn,
	array,
	obj,
	str
};

export default dataTypes;