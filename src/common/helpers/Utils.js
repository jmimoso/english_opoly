/**
 * generates an universal unique identifier
 * @returns {String}
 */
export const generateUUID = () => {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
		const r = Math.random() * 16 | 0;
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
}; 

export const detectBrowser = () => {
	var ua = navigator.userAgent;
	var tem;
	var M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

	if (/trident/i.test(M[1])) {
		tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
		return 'IE ' + (tem[1] || '');
	};

	if (M[1] === 'Chrome') {
		tem = ua.match(/\b(OPR|Edge)\/(\d+)/);

		if (tem != null) {
			return tem.slice(1).join(' ').replace('OPR', 'Opera');
		};
	};

	M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];

	if ((tem = ua.match(/version\/(\d+)/i)) != null) {
		M.splice(1, 1, tem[1]);
	};

	return M.join(' ');
};

export const isIE = () => detectBrowser().toUpperCase().indexOf('IE') > -1;

export const getBrowserLanguage = () => navigator.language || navigator.browserLanguage;

export default{
	generateUUID,
	detectBrowser,
	isIE,
	getBrowserLanguage
};