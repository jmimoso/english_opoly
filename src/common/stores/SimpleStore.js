import Log from 'handlers/DebugHandler';
import BaseStore from 'stores/base/BaseStore';

class SimpleStore extends BaseStore {
	constructor(props) {
		super(props);
		return this;
	}

	buildInitState() {
		return {
			text: ''
		};
	}

	getText() {
		return this.get('text');
	}
};

export default SimpleStore;