import Log from 'handlers/DebugHandler';
import BaseStore from './BaseStore';

class BaseStoreWithHistory extends BaseStore{
	constructor(props) {
		super(props);

		this._dataHistory = [];
		return this;
	}

	storeData(newData) {
		this._data = newData || this.buildInitState();
		this._dataHistory.push(this._data);
		this.emitChange(this._subscribersStorageKey, this.storedData);
		return this;
	}
};

export default BaseStoreWithHistory;