import BabelPolyfill from 'babel-polyfill';
import 'raf/polyfill';

export default () => ({
	BabelPolyfill,
	Raf
});