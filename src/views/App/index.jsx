import React, { Component } from 'react';
import Route from 'react-router-dom/Route';
import Link from 'react-router-dom/Link';


import Utils from 'helpers/Utils';

import Translation from 'components/Translation';
import Entry from 'views/Entry';

class AppController extends Component {
	constructor(props) {
		super(props);

		this.state = this.buildState();
		return this;
	}
	componentDidMount() {
		return this;
	}

	componentWillUnmount() {
		return this;
	}

	buildState(nextProps) {
		return {
		};
	}

	render() {

		return (
			<div className='main-container'>
				<Route
					exact
					path={'/'}
					component={Entry}>
				</Route>
			</div>
		);
	} 
};

export default AppController;