import React, { Component } from 'react';
import ReactDice from 'react-dice-complete';
import PinView from './PinView';

import 'react-dice-complete/dist/react-dice-complete.css'

import Utils from 'helpers/Utils';


class EntryController extends Component {


	constructor(props) {
		super(props);
		this.state = {
			pins: [{
				name: 'pino1'
			}, {
				name: 'pino2'
			}, {
				name: 'pino3'
			}, {
				name: 'pino4'
			}, {
				name: 'pino5'
			}, {
				name: 'pino6'
			}]
		};
		return this;
	}

	componentWillMount() {
		return this;
	}

	componentWillUnmount() {
		return this;
	}

	render() {
		return (
			<div className='entry-container'>
				<div className='entry-wrapper'>
					{this._renderDices()}
					{this._renderPlayerPins()}
				</div>
			</div>
		);
	}

	_renderPlayerPins() {
		return this.state.pins.map((pin, key) => {
			return (
				<PinView key={key} pin={pin} index={key} />
			);
		});
	}

	_renderDices() {
		return(
			<div className='dice-wrapper' onClick={this.rollAll}>
				<ReactDice
					numDice={2}
					rollDone={this.rollDoneCallback}
					disableIndividual={true}
					faceColor={'#EFEFEF'}
					dotColor={'#231F20'}
					margin={30}
					ref={dice => this.dice = dice}>
				</ReactDice>
			</div>
		);
	}

	rollAll = () => {
    	this.dice.rollAll()
  	}


};

export default EntryController;