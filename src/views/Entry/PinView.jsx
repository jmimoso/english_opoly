import React, { Component } from 'react';
import Draggable from 'gsap/Draggable';

import Translation from 'components/Translation';

export default class PinView extends Component {

	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
		Draggable.create(this.pin, {
			edgeResistance: 0.5,
			type: 'x,y',
			throwProps: true
		});
		return this;
	}

	render () {
		return (
			<div ref={pin => this.pin = pin} className='pin' style={this.pinStyles()}></div>
		);
	}

	pinStyles() {
		if(this.props.index === 5) {
			return {
				height: '10%',
				top: '90%',
				backgroundImage: `url(${require('images/' + this.props.pin.name + '.png')})`
			};
		} else {
			return {
				top: (this.props.index * 18) + '%',
				backgroundImage: `url(${require('images/' + this.props.pin.name + '.png')})`
			}
		}
	}
	

};