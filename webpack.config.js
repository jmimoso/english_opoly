const { resolve, join } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const StringReplacePlugin = require('string-replace-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const { devLog } = require('ui-building-helper/lib/debugOutup');
const { WebpackHandler, EnvironmentHandler } = require('ui-building-helper');

const browsers = ['last 2 versions', 'ie >= 9'];

const webpackConfig = (basePath, context, output, env) => {
	return {
		mode: EnvironmentHandler.getEnvType(),

		context: context,

		entry: {
			main: './main.js'
		},

		output: {
			path: join(basePath, output),
			publicPath: '/',
			filename: 'js/[name].bundle.js',
			chunkFilename: 'js/[name].bundle.js'
		},

		resolve: {
			extensions: ['.js', '.jsx'],
			
			alias: {
				Log: join(context, 'common/handlers/DebugHandler'),
				
				root: join(context),
				common: join(context, 'common/'),
				components: join(context, 'components/'),
				views: join(context, 'views/'),
				
				audios: join(context, 'assets/audios/'),
				configurations: join(context, 'assets/configurations/'),
				images: join(context, 'assets/images/'),
				locales: join(context, 'assets/locales/'),

				collections: join(context, 'common/collections/'),
				facades: join(context, 'common/facades/'),
				factories: join(context, 'common/factories/'),
				filters: join(context, 'common/filters/'),
				handlers: join(context, 'common/handlers/'),
				helpers: join(context, 'common/helpers/'),
				mixins: join(context, 'common/mixins/'),
				reducers: join(context, 'common/reducers/'),
				services: join(context, 'common/services/'),
				stores: join(context, 'common/stores/'),
				validators: join(context, 'common/validators/')
			}
		},

		module: {
			rules: [
				{
					test: /\.(jsx|js)$/,
					use: [
						{
							loader: 'babel-loader',
							options: {
								presets: [
									[
										'env', {
											modules: false,
											debug: true,
											targets: {
												browsers,
												uglify: true
											}
										}
									],
									'react'
								],
								plugins: [
									'syntax-dynamic-import', //This will enabel async import('<module>').then(...)
									'transform-runtime', // This will avoid the duplication of babel helpers
									'transform-class-properties' //This will compile arrow funcs
								]	
							}
						}
					],
					exclude: [/node_modules/, /test/]
				},
				{
					test: /TranslationsHandler.js/,
					use: [
						{
							loader: StringReplacePlugin.replace({
								replacements: [{
									pattern: /<!-- @defaultLng (\w*?) -->/ig,
									replacement: (match, p1, offset, string) => WebpackHandler.getDefinitionsProp(p1)
								}]
							})
						}
					]
				},
				{
					test: /\.css$/,
					use: ExtractTextPlugin.extract({
						fallback: "style-loader",
						publicPath: '../',
						use: [
							{
								loader: "css-loader",
								options: { importLoaders: 1, sourceMap: true }
							},
							{
								loader: "postcss-loader",
								options: { map: false, sourceMap: true, browsers }
							}
						]
					})
				},
				{
					test: /\.scss$/,
					use: ExtractTextPlugin.extract({
						fallback: "style-loader",
						publicPath: '../',
						use: [
							{
								loader: "css-loader",
								options: { importLoaders: 1, sourceMap: true }
							},
							{
								loader: "postcss-loader",
								options: { map: false, sourceMap: true, browsers }
							},
							{
								loader: "sass-loader?sourceMap",
								options: { sourceMap: true }
							}
						]
					})
				},
				{
					test: /\.(svg|ttf|woff|woff2|eot)(\?v=\d+\.\d+\.\d+)?$/i,
					exclude: [/images/],
					loader: ['url-loader?name=assets/fonts/[name].[ext]&limit=10000']
				},
				{
					test: /\.mp3$/,
					use: ['file-loader?name=assets/audios/[name].[ext]&limit=10000&mimetype=audio/mpeg']
				}
			]
		},

		plugins: [
			new CleanWebpackPlugin(['index.js', output], { 'root': basePath }),

			new StringReplacePlugin(),

			new webpack.EnvironmentPlugin(['NODE_ENV']),

			new webpack.DefinePlugin(WebpackHandler.getConfigsForDefinePlugin()),
			
			new webpack.ProvidePlugin({
				'Log': 'Log'
			}),

			new ExtractTextPlugin("css/[name].css"),

			new HtmlWebpackPlugin({
				title: env.npm_package_name,
				template: './index.html',
				lang: WebpackHandler.getDefinitionsProp('Intl_default_language'),
				appMountId: 'root'
			})
		],

		stats: {
			colors: true
		}
	};
};

module.exports = (() => WebpackHandler.envify(webpackConfig))();