# [NAME]

[DESC] ...

### Tech

[NAME] uses a number of open source projects to work properly:

* [React](https://facebook.github.io/react/) 
* [Flux](https://facebook.github.io/flux/) 


### Start Project

$ npm init -y

### Installation

```sh
$ cd [PROJECT_DIR]
$ npm install
$ npm run dev
$ npm run build
```

For production environments...

```sh
$ cd [PROJECT_DIR]
$ npm install -d
$ npm start
```
